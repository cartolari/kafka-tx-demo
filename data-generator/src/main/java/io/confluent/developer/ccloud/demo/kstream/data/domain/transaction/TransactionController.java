package io.confluent.developer.ccloud.demo.kstream.data.domain.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class TransactionController {
  private final TransactionService transactionService;

  @PostMapping
  public Mono<Void> publishTransaction(@RequestBody TransactionRequest transactionRequest) {
    return transactionService.publishTransaction(transactionRequest);
  }
}
